/**
 * Test coverage:
 *
 * - Main GET cases
 * - Main POST cases
 *
 * (any other cases for PUT and DELETE will be just similar to covered ones)
 */

jest.dontMock('../index');
var xhr = require('../index');

var openMock = XMLHttpRequest.prototype.open = jest.genMockFunction();
var setRequestHeaderMock = XMLHttpRequest.prototype.setRequestHeader = jest.genMockFunction();
var sendMock = XMLHttpRequest.prototype.send = jest.genMockFunction();

describe('xhr.js', function () {
  // GET
  describe('GET', function () {
    afterEach(function () {
      openMock.mockClear();
      setRequestHeaderMock.mockClear();
      sendMock.mockClear();
    });

    it('has GET public method', function () {
      expect(xhr.GET).toEqual(jasmine.any(Function));
    });

    it('throws an Error if the URL is not provided', function () {
      expect(function () {
        xhr.GET({});
      }).toThrow(new Error('[xhr.js]: The URL is invalid or missing!'));
    });

    it('initializes GET request with the right params', function () {
      xhr.GET({
        url: 'https://api.github.com/gists/aa5a315d61ae9438b18d'
      });

      expect(openMock.mock.calls[0][0]).toEqual('GET');
      expect(openMock.mock.calls[0][1]).toEqual('https://api.github.com/gists/aa5a315d61ae9438b18d');
    });

    it('sets "application/json" Accept header if `responseFormat` property is not provided', function () {
      xhr.GET({
        url: 'https://api.github.com/gists/aa5a315d61ae9438b18d'
      });

      expect(setRequestHeaderMock.mock.calls[0][1]).toEqual('application/json');
    });

    it('sets Accept header provided in `responseFormat` property', function () {
      xhr.GET({
        url: 'https://api.github.com/gists/aa5a315d61ae9438b18d',
        responseFormat: 'text/plain'
      });

      expect(setRequestHeaderMock.mock.calls[0][1]).toEqual('text/plain');
    });

    it('sets provided custom request headers', function () {
      var header1Key,
          header1Value,
          header2Key,
          header2Value;

      xhr.GET({
        url: 'https://api.github.com/gists/aa5a315d61ae9438b18d',
        requestHeaders: {
          customHeader1Key: 'customHeader1Value',
          customHeader2Key: 'customHeader2Value'
        }
      })

      header1Key = setRequestHeaderMock.mock.calls[0][0];
      header1Value = setRequestHeaderMock.mock.calls[0][1];
      header2Key = setRequestHeaderMock.mock.calls[1][0];
      header2Value = setRequestHeaderMock.mock.calls[1][1];

      expect(header1Key).toEqual('customHeader1Key');
      expect(header1Value).toEqual('customHeader1Value');
      expect(header2Key).toEqual('customHeader2Key');
      expect(header2Value).toEqual('customHeader2Value');
    });

    it('sends empty data string for GET method', function () {
      xhr.GET({
        url: 'https://api.github.com/gists/706e4899b785f4f9fc1a'
      })

      expect(sendMock.mock.calls[0][0]).toEqual('');
    });

    it('calls `success` callback when the server response is successful', function () {
      xhr.GET({
        url: 'https://api.github.com/gists/706e4899b785f4f9fc1a',
        success: function (response) {
          expect(JSON.parse(arguments[0])).toBe(Object);
        }
      });
    });

    it('calls `error` callback when the server response fails', function () {
      xhr.GET({
        url: 'https://api.github.com/gists/aa5a315d61ae9438b18d',
        error: function (response) {
          expect(arguments[0]).toEqual('Not Found');
        }
      });
    });
  });

  // POST
  describe('POST', function () {
    afterEach(function () {
      openMock.mockClear();
      setRequestHeaderMock.mockClear();
      sendMock.mockClear();
    });

    it('has POST public method', function () {
      expect(xhr.POST).toEqual(jasmine.any(Function));
    });

    it('throws an Error if the URL is not provided', function () {
      expect(function () {
        xhr.POST({});
      }).toThrow(new Error('[xhr.js]: The URL is invalid or missing!'));
    });

    it('sets "application/x-www-form-urlencoded" Content-Type header by default', function () {
      xhr.POST({
        url: 'https://api.github.com/gists/aa5a315d61ae9438b18d',
        data: {
          a: 123
        }
      });

      expect(setRequestHeaderMock.mock.calls[2][1]).toEqual('application/x-www-form-urlencoded');
    });

    it('sets "application/json" Accept header if `responseFormat` property is not provided', function () {
      xhr.POST({
        url: 'https://api.github.com/gists/aa5a315d61ae9438b18d'
      });

      expect(setRequestHeaderMock.mock.calls[0][1]).toEqual('application/json');
    });

    it('sets Accept header provided in `responseFormat` property', function () {
      xhr.POST({
        url: 'https://api.github.com/gists/aa5a315d61ae9438b18d',
        responseFormat: 'text/plain'
      });

      expect(setRequestHeaderMock.mock.calls[0][1]).toEqual('text/plain');
    });

    it('set provided custom request headers', function () {
      var header1Key,
          header1Value,
          header2Key,
          header2Value;

      xhr.POST({
        url: 'https://api.github.com/gists/aa5a315d61ae9438b18d',
        requestHeaders: {
          customHeader1Key: 'customHeader1Value',
          customHeader2Key: 'customHeader2Value'
        }
      })

      header1Key = setRequestHeaderMock.mock.calls[0][0];
      header1Value = setRequestHeaderMock.mock.calls[0][1];
      header2Key = setRequestHeaderMock.mock.calls[1][0];
      header2Value = setRequestHeaderMock.mock.calls[1][1];

      expect(header1Key).toEqual('customHeader1Key');
      expect(header1Value).toEqual('customHeader1Value');
      expect(header2Key).toEqual('customHeader2Key');
      expect(header2Value).toEqual('customHeader2Value');
    });

    it('sends data provided', function () {
      var data = {
          sample: 123
      };

      xhr.POST({
        url: 'https://api.github.com/gists/aa5a315d61ae9438b18d',
        data: data
      });

      expect(sendMock.mock.calls[0][0]).toBe(data);
    });

    it('calls `error` callback when the server response fails', function () {
      xhr.POST({
        url: 'https://api.github.com/gists/aa5a315d61ae9438b18d',
        data: {
          sample: 123
        },
        error: function (response) {
          expect(arguments[0]).toEqual('Not Found');
        }
      });
    });
  });
});