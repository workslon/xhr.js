[TOC]

# Overview

`xhr.js` is a browser library, that allows to perform basic ajax requests.
It is an [Universal Module](https://github.com/umdjs/umd) that supports following Module Systems:

+ [AMD](http://requirejs.org/docs/whyamd.html)

+ [CommonJS](https://en.wikipedia.org/wiki/CommonJS)

+ Globals - Adds to `window` in web browsers

**NOTE:** `xhr.js` can be used only in browser, but can be `required` for the build process only within the NodeJS project (CommonJS).

# Usage

## Browser

+ Add `<script>` with `xhr.js` library to the page

+ `window.xhr` variable will be available for use

## CommonJS

+ Run `npm install https://git@bitbucket.org/workslon/xhr.js.git` or `git+ssh://git@bitbucket.org/workslon/xhr.js.git` in the command-line - this will install the module and add it to the `node_modules` folder of your project

+ `var xhr = require('xhr');` from any file

## AMD (RequireJS)

+ Add library to the project

+ Config RequireJS

+ `var xhr = require('xhr');` from any file


# API References

## Methods:

### GET(params)

__Parameters__

| Name   | Type   | Description        |
|--------|--------|--------------------|
| params | Object | Request parameters |

### POST(params)

__Parameters__

| Name   | Type   | Description        |
|--------|--------|--------------------|
| params | Object | Request parameters |

### PUT(params)

__Parameters__

| Name   | Type   | Description        |
|--------|--------|--------------------|
| params | Object | Request parameters |

### DELETE(params)

__Parameters__

| Name   | Type   | Description        |
|--------|--------|--------------------|
| params | Object | Request parameters |

### OPTIONS(params)

__Parameters__

| Name   | Type   | Description        |
|--------|--------|--------------------|
| params | Object | Request parameters |

## Available Parameters

| Name            | Type      | Description                                             |   |
|-----------------|-----------|---------------------------------------------------------|---|
| url             | String    | request URL                                             |   |
| data            | Object    | data to be sent (relevant to 'POST' and 'PUT' requests) |   |
| requestFormat   | String    | request `contentType` header                            |   |
| responseFormat  | String    | request `Accept` header                                 |   |
| requestHeaders  | Object    | request Headers                                         |   |
| success         | Function  | request success handler                                 |   |
| error           | Function  | request error handler                                   |   |

# Examples

### POST

```javascript
xhr.POST({
  url: 'https://api.github.com/gists/5439650238ab6b317fe9',
  data: {
    isbn: 123456789,
    title: 'Book',
    year: 3000
  },
  success: function (response) {
    console.log(response);
  },
  error: function (error) {
    console.warn(error);
  }
});

```
### GET

```javascript
xhr.GET({
  url: 'https://api.github.com/gists/5439650238ab6b317fe9',
  success: function (response) {
    console.log(response);
  },
  error: function (error) {
    console.warn(error);
  }
});

```

# Run Tests

+ run `npm install` to install [Jest](https://facebook.github.io/jest) (unit testing module)

+ run `npm test` to see tests passed