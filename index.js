(function (name, root, factory) {
  if (typeof module !== 'undefined' && module.exports) module.exports = factory();
  else if (typeof define === 'function' && define.amd) define(factory);
  else root[name] = factory();
}('xhr', this, function () {
  return (function () {
    function GET(params) {
      params = params || {};
      params.method = 'GET';
      return makeRequest(params);
    }

    function PUT(params) {
      params = params || {};
      params.method = 'PUT';
      makeRequest(params);
    }

    function POST(params) {
      params = params || {};
      params.method = 'POST';
      makeRequest(params);
    }

    function DELETE(params) {
      params = params || {};
      params.method = 'DELETE';
      makeRequest(params);
    }

    function OPTIONS(params) {
      params = params || {};
      params.method = 'OPTIONS';
      makeRequest(params);
    }

    function isValidUrl(url) {
      var URL_PATTERN = /\b(https?):\/\/[\-A-Za-z0-9+&@#\/%?=~_|!:,.;]*[\-A-Za-z0-9+&@#\/%=~_|??]/;
      return url && URL_PATTERN.test(url);
    }

    function makeRequest(requestParams) {
      var params = requestParams || {},
          request = new window.XMLHttpRequest(),
          url = params.url,
          data = params.data || {},
          method = params.method || 'GET',
          requestFormat = params.requestFormat || 'application/x-www-form-urlencoded',
          responseFormat = params.responseFormat || 'application/json',
          requestHeaders = requestHeaders || [],
          successHandler = params.success || function () {},
          errorHandler = params.error || function () {};

      if (!isValidUrl(params.url)) {
        throw new Error('[xhr.js]: The URL is invalid or missing!');
      }

      request.open(method, url, true);

      request.onreadystatechange = function () {
        if (request.readyState === XMLHttpRequest.DONE) {
          if (request.status >= 400) {
            errorHandler.call(request, request.statusText);
          } else {
            successHandler.call(request, request.responseText);
          }
        }
      };

      if (params.requestHeaders) {
        Object.keys(params.requestHeaders).forEach(function (header) {
          request.setRequestHeader(header, params.requestHeaders[header]);
        });
      }

      request.setRequestHeader("Accept", responseFormat);
      request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

      if (method === "GET" || method === "DELETE") {
        request.send('');
      } else {
        request.setRequestHeader("Content-Type", requestFormat);
        request.send(data);
      }
    }

    return {
      GET: GET,
      POST: POST,
      PUT: PUT,
      DELETE: DELETE,
      OPTIONS: OPTIONS
    }
  })();
}));
